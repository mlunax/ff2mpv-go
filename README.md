ff2mpv-go
=========

Native messaging host for [ff2mpv](https://github.com/woodruffw/ff2mpv/) written in Go.

Since it is a native binary, it starts faster than the official Python or Ruby
implementations.


Requirements
------------

- [Firefox](https://www.mozilla.org/en-US/firefox/) 50 or later
- [ff2mpv](https://github.com/woodruffw/ff2mpv/) 3.0 or later
- [Go](https://golang.org/) 11 or later (probably works on older versions too)
- [mpv](https://mpv.io/) 0.21.0 or later


Installation
------------

- `go install gitlab.com/mlunax/ff2mpv-go@latest`
- `ff2mpv-go --manifest > ~/.mozilla/native-messaging-hosts/ff2mpv.json`

If `$GOBIN` is not in `$PATH`, specify the full path to the installed
executable (e.g. `$GOBIN/ff2mpv-go`) when running it.

If a custom mpv command is required, manually clone the repository and edit the
`mpvCmd` variable in `ff2mpv.go`, then run `go install` in the package folder
instead of above the `go install` step.


Usage
-----

Use ff2mpv as normally.
